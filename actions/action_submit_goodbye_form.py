
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker, Action
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
import json
import os
from datetime import datetime
import logging
import re
from lib.db_queries import *
from lib.resume import *

class SubmitGoodByeForm(Action):
	"""Example of a custom form action"""

	def name(self):
		# type: () -> Text
		"""Unique identifier of the form"""
		return "action_submit_goodbye_form"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict]:
		try:
			intent_name = "st_goodbye"
			#Get the tracker id from the tracker
			tracker_id = tracker.sender_id
			#Update the resume list
			goodbye_confirmation = get_slot(tracker_id,"goodbye_confirmation")
			if goodbye_confirmation == "yes":
				db_slots = {}
				db_slots['resumable'] = []
				db_slots['goodbye_confirmation'] = None
				set_multiple_slots(tracker_id,db_slots)
				message = (
						[
							{
								"message": {
									"template": {
										"elements": {
											"title" : "Thank you.",
										},
										"type": "Card",
									}
								}
							},
							{"ssml": "<speak>Thank you.</speak>"}
						]
					)
				dispatcher.utter_message(json.dumps(message))
				return [FollowupAction('action_listen'),SlotSet('goodbye_confirmation',None)]
			else:
				data = resumeForm(sender_id=tracker_id,current_form_name="goodbye_form")
				print("the data to resume is")
				print(data)
				#Response message for the current form
				response_message = None
				#Clears the form slots and also sets some additional slots in db.
				slots = clearSlot(sender_id=tracker_id,intent_name=intent_name,data=data,response_message=response_message)
				return [FollowupAction('resume_form'),SlotSet("resume_confirmation",data.get("resume_confirmation"))] + slots
		except BaseException as e:
			print(e)
			ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
			fallback_message = '[{ "message": { "template": { "elements": { "title": "I am sorry! I am still learning and I could not understand what you said. Could you please say that again?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak>' + ssml + '</speak>"}]'
			dispatcher.utter_message(fallback_message)
			return []