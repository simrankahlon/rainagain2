
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker, Action
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
import json
import os
from datetime import datetime
import logging
import re
from lib.db_queries import *
from lib.resume import *

class SubmitAskToSwitchForm(Action):
	"""Example of a custom form action"""

	def name(self):
		# type: () -> Text
		"""Unique identifier of the form"""
		return "action_submit_ask_to_switch_form"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict]:
		try:
			#Get the current intent name
			intent_name = tracker.latest_message["intent"]["name"]
			#Get the value for switch confirmation slot
			switch_confirmation = tracker.get_slot('switch_confirmation')
			#Get the tracker id from the tracker
			tracker_id = tracker.sender_id
			#Get the resume intent list from Database
			resume_list_response = getResumableList(sender_id=tracker_id)
			if resume_list_response.get("success") == 1:
				resume_intent_list = resume_list_response.get("data")
			else:
				resume_intent_list = None
			db_slots = {}
			db_slots["switch_confirmation"] = None
			#If the user wishes to switch
			if switch_confirmation == "yes":
				intent_name = get_slot(tracker_id,"current_intent")
				#Static file that contains intent action mapping
				with open('./lib/intent_details.json', 'r') as f:
					message = json.load(f)
				#Get the intent details from json
				intent_details = message[intent_name]
				#Get the clear slot on resume field
				clear_slots_on_resume = intent_details.get("clear_slots_on_resume")
				slots = []
				#On resume start from first slot, clear all the prefilled slots.
				if clear_slots_on_resume == "yes":
					intent_slots = intent_details.get("slots")
					for i in intent_slots:
						slots.append(SlotSet(i,None))
						db_slots[i] = None
				#Add the intent in the resume intent list
				resume_intent_list =  addIntentToResumableList(intent_name,intent_details,resume_intent_list)
				action_to_resume_on_yes = resume_intent_list[-1].get("form")
				#Replace the resume list in the db again
				replaceResumableList(sender_id=tracker_id,data=resume_intent_list)
				set_multiple_slots(tracker_id,db_slots)
				return [FollowupAction(action_to_resume_on_yes),SlotSet("switch_confirmation",None)]+slots
			#If the user doesnt wish to switch, resume the current form and remove the other one from the resume list
			elif switch_confirmation == "no":
				action_to_resume_on_no = resume_intent_list[-1].get("form")
				#Replace the resume list in the db again
				replaceResumableList(sender_id=tracker_id,data=resume_intent_list)
				set_multiple_slots(tracker_id,db_slots)
				return [FollowupAction(action_to_resume_on_no),SlotSet("switch_confirmation",None)]
		except BaseException as e:
			print(e)
			ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
			fallback_message = '[{ "message": { "template": { "elements": { "title": "I am sorry! I am still learning and I could not understand what you said. Could you please say that again?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak>' + ssml + '</speak>"}]'
			dispatcher.utter_message(fallback_message)
			return []